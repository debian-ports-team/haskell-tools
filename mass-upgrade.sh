#!/bin/bash

if [ "$1" = "-D" ]
then
	dist="$2"
	shift
	shift
fi

if [ -z "$1" -o "$1" = '--help' ]
then
	echo "Usage: $0 [-D distribution] dirs..."
	echo
	echo "Changes the version number to the latest version found via uscan"
	echo "(i.e. the debian/watch file)."
	echo
	echo "ATTENTION: You need to manually check build-dependencies, patches etc."
	echo "For haskell packages, http://hdiff.luite.com/cgit/<pkg>/ is useful."
	exit
fi

set -e
status=0

while [ -n "$1" ]
do
	dir=$1
	shift

	if ! pushd "$dir" >/dev/null
	then
		echo "Failed to switch to \"$dir\""
		status=1
		continue
	fi

	if [ ! -e changelog ]
	then
		echo "No changelog file found, skipping $dir"
		popd >/dev/null
		status=1
		continue
	fi

	if [ ! -e watch ]
	then
		echo "No watch file found, skipping $dir"
		popd >/dev/null
		status=1
		continue
	fi

	if darcs whatsnew -s >/dev/null
	then
		echo "Unrecorded changes in $dir, skipping:"
		darcs diff | colordiff
		popd
		status=1
		continue
	fi

	package="$(grep-dctrl -n -s Source . < control)"
	version="$(uscan --dehs --upstream-version 0 --watchfile watch --package $package --report-status|xpath -e 'dehs/upstream-version/text()' -q)"
	if [ -z "$version" ]
	then
		echo "could not detect version."
		popd >/dev/null
		status=1
		continue
	fi
	version="$version-1"

	echo "Upgrading $dir to version $version."

	debchange --changelog changelog --newversion="$version" 'New upstream release'
	debcommit

	popd >/dev/null
done

exit $status
