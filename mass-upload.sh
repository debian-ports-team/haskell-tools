#!/bin/bash

if [ -z "$1" -o "$1" = '--help' ]
then
	echo "Usage: $0 [-k keyid] repodir/ file.changes file.changes..."
	echo
	echo "For each of the changes, will"
	echo " * sign it"
	echo " * dput it"
	echo " * find the appropriate darcs repository in repodir/<sourcename>"
	echo " * pushes that"
	echo " * and renames the file to file.changes-done"
	exit
fi

if [ "$1" = "-k" ];
then
	shift;
	debsignarg="-k $1";
	shift;
fi

repodir=$1
shift

set -e
status=0

while [ -n "$1" ]
do
	changes=$1
	shift

	#pkg="$(grep-dctrl '' -s Source -n "$changes")"
	pkg="$(grep ^Source $changes | cut -c9-)"

	if [ ! -d "$repodir/$pkg/_darcs" ]
	then
		echo "Skipping $changes, $repodir/$pkg not found or not a darcs repo"
		continue
	fi

	debsign --re-sign $debsignarg "$changes"
	dput ssh-upload "$changes"
	darcs push --repo="$repodir/$pkg" -a
	mv "$changes" "$changes-done"

done

exit $status
