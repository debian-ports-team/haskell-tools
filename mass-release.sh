#!/bin/bash

if [ "$1" = "-D" ]
then
	dist="$2"
	shift
	shift
fi

if [ -z "$1" -o "$1" = '--help' ]
then
	echo "Usage: $0 [-D distribution] dirs..."
	echo
	echo "Calls debchange -r on all repos that are not already released"
	echo "(i.e. distribution is UNRELEASED)."
	exit
fi

set -e

while [ -n "$1" ]
do
	dir=$1
	shift

	if ! pushd "$dir" >/dev/null
	then
		echo "Failed to switch to \"$dir\""
		continue
	fi

	if [ ! -e changelog ]
	then
		echo "No changelog file found, skipping $dir"
		popd >/dev/null
		continue
	fi

	currentdist=$(dpkg-parsechangelog -lchangelog -c1 |grep-dctrl -n -s Distribution .)
	if [ "$currentdist" = "UNRELEASED" ]
	then
		echo "Releasing $dir"
		if [ -n "$dist" ]
		then
			debchange --changelog=changelog  -r '' -D $dist
		else
			debchange --changelog=changelog  -r ''
		fi
	else
		echo "Skipping $dir, already released."
	fi
	popd >/dev/null
done
