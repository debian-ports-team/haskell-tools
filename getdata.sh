#!/bin/sh
mirror="http://cdn.debian.net/debian"
distributions="unstable"
components="main"
#architectures="amd64 i386 alpha powerpc armel"
architectures="alpha amd64 armel hppa i386 ia64 mips mipsel powerpc s390 sparc kfreebsd-amd64 kfreebsd-i386"

base="$mirror/dists"
test -d data/ || mkdir data/
cd data/
for distro in $distributions ; do
  for comp in $components ; do
    fname="$distro-$comp-Sources.gz"
    if ! test -L $fname
    then
      echo "Getting $fname..."
      curl -o $fname -z $fname $base/$distro/$comp/source/Sources.gz
      for arch in $architectures ; do
        fname="$distro-$comp-binary-$arch-Packages.gz"
        echo "Getting $fname..."
        curl -o $fname -z $fname $base/$distro/$comp/binary-$arch/Packages.gz
      done
    fi 
  done
done

for arch in $architectures ; do
  fname="wanna-build-dump-$arch.gz"
  if ! test -L $fname
  then
    echo "Getting $fname..."
    curl -o $fname -z $fname https://buildd.debian.org/stats/$arch-dump.txt.gz
  fi 
done

